import csv
import logging
import math
import matplotlib.pyplot as plt
import random
import time

class Perceptron(object):
  def __init__(self, size=2, mu=0.2):
    self.mu = mu
    self.size = size
    self.ws = [random.random() for i in range(size)]

  def guess(self, v):
    dot = sum([w * v[i] for i, w in enumerate(self.ws)])
    if dot > 0:
      act = 1
    elif dot < 0:
      act = -1
    else:
      act = 0

    return act

  def train(self, training_set, iter=100):
    for n in range(iter):
      v, expected = random.choice(training_set)
      
      dot = sum([w * v[i] for i, w in enumerate(self.ws)])
      act = 1 if dot >= 0 else -1

      err = expected - act

      # print "{0:2.2f} {1:2.2f} => {2} | {3}".format(v[1], v[2], act, expected)

      self.ws = [w + self.mu * err * v[i] for i, w in enumerate(self.ws)]

class Pocket(Perceptron):
  def __init__(self, size=2, mu=0.1):
    super(Pocket, self).__init__(size, mu)

  def train(self, training_set, iter=10000):
    runw = runx = num_okw = num_okx = 0
    pocket = self.ws

    for n in range(iter):
      v, expected = random.choice(training_set)
      if self.guess(v) == expected:
        runx += 1

        if runx > runw:
          num_okx = 0
          for d in training_set:
            if self.guess(d[0]) == d[1]:
              num_okx += 1
          if num_okx > num_okw:
            pocket = self.ws
            runw = runx
            num_okw = num_okx
      else:
        dot = sum([w * v[i] for i, w in enumerate(self.ws)])
        act = 1 if dot >= 0 else -1
        err = expected - act

        self.ws = [w + self.mu * err * v[i] for i, w in enumerate(self.ws)]
        runw = 0

      # time.sleep(1)
      # print num_okw

    self.ws = pocket
      

class AdaBoost:
  def __init__(self):
    self.M = []
    # self.M_e = []
    self.M_a = []

  def guess(self, v, K=False):
    ml = self.M
    if K:
      ml = self.M[:K]
    return math.copysign(1, 
      sum([self.M_a[i] * m.guess(v) for i, m in enumerate(ml)]))

  def train(self, training_set, K=5, generator=Perceptron):
    self.training_set = training_set
    self.generator = generator
    self.K = K

    input_len = len(self.training_set[0][0])
    D = [1.0 / len(self.training_set)] * len(self.training_set)

    t = self.K
    while t > 0:
      # Sample select
      Di = []
      for _ in range(len(self.training_set)):
        r = random.random()
        a = 0
        for i, data in enumerate(self.training_set):
          a += D[i]
          if r <= a:
            Di.append(i)
            break

      # Train
      training_set_t = [self.training_set[i] for i in Di]
      p = self.generator(input_len)
      p.train(training_set_t)

      # guess * expected
      G = []

      # Get err rate
      e_t = 0.001
      for i, (v, expected) in enumerate(self.training_set):
        output = p.guess(v)
        G.append(output * expected)
        if output != expected:
          e_t += D[i]

      if e_t > 0.5:
        continue

      # self.M_e.append(e_t)

      a_t = 0.5 * math.log((1 - e_t) / e_t)
      self.M_a.append(a_t)

      # Compute new probs
      D = [
        d * math.exp(-a_t * G[i])
        for i, d in enumerate(D)
      ]

      D_sum = sum(D)
      D = [d / D_sum for d in D]

      self.M.append(p)

      t -= 1
      prog = 10 * (1 - float(t) / self.K)
      prev_prog = 10 * (1 - float(t + 1) / self.K)
      if int(prog) != int(prev_prog):
        print('|', end="", flush=True)
    print()


def load_training_set(filename, bias=False):
  data = []
  o = []

  with open(filename, 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      v = [float(x) for x in row[1:]]
      if bias:
        v = [bias] + v
      data.append(v)
      o.append(float(row[0]))

  return data, o

def gen_gauss_training_set(feature_count, n=100, mu=0, sigma=1, y=1, bias=1):
  data = []
  o = []
  for i in range(n):
    d = [bias] + [random.gauss(mu, sigma) for x in range(feature_count)]
    data.append(d)
    o.append(y)

  return data, o

"""
2. Perceptron Classifier Construction
"""
def classify(sample_set, expected_output_set, generator=Pocket):
  training_set = [[sample_set[i], expected_output_set[i]] for i in range(len(sample_set))]

  p = generator()
  p.train(training_set)

  return p

def predict(sample_set, model):
  result = [model.guess(g) for g in sample_set]

  return result

def perceptron_test():
  I = 1

  data_y1, o_y1 = gen_gauss_training_set(2, n=100, mu=0, sigma=I, y=-1)
  data_y2, o_y2 = gen_gauss_training_set(2, n=100, mu=10, sigma=I, y=1)

  m = classify(data_y1[:50] + data_y2[:50], o_y1[:50] + o_y2[:50], generator=Pocket)

  g = predict(data_y1[50:] + data_y2[50:], m)

  e = o_y1[50:] + o_y2[50:]

  s = [1 if e[i] == g[i] else 0 for i in range(50)]

  print('{:.2%}'.format(sum(s) / 50.0))

"""
3. Adaboost Construction and Evalutaion
"""
def adabtrain(sample_set, expected_output_set, generator=Pocket, K=1):
  training_set = [[sample_set[i], expected_output_set[i]] for i in range(len(sample_set))]
  # print(training_set)

  p = AdaBoost()
  p.train(training_set, generator=generator, K=K)

  return p

def adabpredict(sample_set, model, K=False):
  if K:
    result = [model.guess(g, K) for g in sample_set]
  else:
    result = [model.guess(g) for g in sample_set]

  return result

def adaboost_test(T=10):
  data, o = load_training_set('data/splice_data.csv', 1)

  limit = 400

  results = []

  for K in range(10, 1001, 10):
    sample = random.sample([list(t) for t in zip(data, o)], limit)
    t_data, t_o = [list(t) for t in zip(*sample)]

    start_time = time.time()

    m = adabtrain(t_data, t_o, generator=Pocket, K=K)

    train_time = time.time() - start_time
    start_time = time.time()

    g = adabpredict(t_data, m)

    predict_time = time.time() - start_time

    e = t_o

    s = [1 if e[i] == g[i] else 0 for i in range(len(e))]
    accuracy = sum(s) / len(e)

    print('K: {}, acc: {:.2%}'.format(K, accuracy))
    results.append((K, accuracy, train_time, predict_time))

  print(results)

  plot(results)

"""
*. Util
"""
def plot(results, results2=[], title="Plot of K vs accuracy"):
  ll = [list(t) for t in zip(*results)]
  k, acc = ll[:2]
  ll2 = [list(t) for t in zip(*results2)]
  _, acc2 = ll2[:2]
  plt.plot(k, acc, 'ro', k, acc2, 'b^')
  plt.title(title)
  plt.xlabel('Number of instance (K)')
  plt.ylabel('Accuracy')
  plt.axis([10, 1000, 0, 1])
  plt.show()


if __name__ == "__main__":
  # perceptron_test()
  adaboost_test()