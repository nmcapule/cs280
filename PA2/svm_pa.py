from libsvm.svmutil import *
import time
import random

"""
4. SVM Classifier
"""
if __name__ == "__main__":
  y, x = svm_read_problem("data/splice_data.libsvm")

  limit = 1000

  results = []

  sample = random.sample([list(t) for t in zip(x, y)], limit)
  t_data, t_o = [list(t) for t in zip(*sample)]

  start_time = time.time()

  m = svm_train(t_o, t_data, "-c 4 -s 1 -t 2")

  train_time = time.time() - start_time
  start_time = time.time()

  p_label, p_acc, p_val = svm_predict(y[:2175], x[:2175], m)

  predict_time = time.time() - start_time

  results.append(p_acc)

  print(results)
  print('Train: {}, Predict: {}'.format(train_time, predict_time))