import csv
import random

def write_csv(filename, data):
  with open(filename, 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    for row in data:
      writer.writerow(row)

def load_csv(filename):
  data = []

  with open(filename, 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      v = [float(x) for x in row]
      data.append(v)

  return data

if __name__ == "__main__":
  input_filename = "data.csv"
  input_label_filename = "data_labels.csv"
  training_set_num = 1800

  data = load_csv(input_filename)
  o = load_csv(input_label_filename)

  ch = {}
  for i in range(len(o)):
    if str(o[i]) not in ch:
      ch[str(o[i])] = []
    ch[str(o[i])].append((data[i], o[i]))

  target_sample_size = len(o) / 8
  data = []
  o = []
  for x in ch:
    artificial_count = target_sample_size - len(ch[x])
    if artificial_count < 0:
      t = random.sample(ch[x], target_sample_size)
      for d in t:
        data.append(d[0])
        o.append(d[1])
      # print(str(x) + '--- ' + str(len(t)))
    else:
      i = 0
      while i < target_sample_size:
        t = random.choice(ch[x])
        data.append(t[0])
        o.append(t[1])

        i += 1

  ch = {}
  for i in range(len(o)):
    if str(o[i]) not in ch:
      ch[str(o[i])] = []
    ch[str(o[i])].append((data[i], o[i]))

  for x in ch:
    print str(x) + ' ' + str(len(ch[x]))

  shuffled_indices = [i for i in range(len(o))]
  random.shuffle(shuffled_indices)

  training_data = ([data[i] 
    for i in shuffled_indices[:training_set_num]])
  training_data_labels = ([o[i]
    for i in shuffled_indices[:training_set_num]])

  write_csv('training_set.csv', training_data)
  write_csv('training_labels.csv', training_data_labels)

  validation_data = ([data[i] 
    for i in shuffled_indices[training_set_num:]])
  validation_data_labels = ([o[i]
    for i in shuffled_indices[training_set_num:]])

  write_csv('validation_set.csv', validation_data)
  write_csv('validation_labels.csv', validation_data_labels)