from libsvm.svmutil import *
import csv
import random
import time

def write_libsvm(data, o, filename):
  with open(filename, 'w') as outfile:
    for i in range(len(o)):
      tokens = []
      tokens.append(str(o[i][0]))
      for j in range(len(data[i])):
        tokens.append(str(j + 1) + ':' + str(data[i][j]))
      s = ' '.join(tokens)
      outfile.write(s + '\n')

def write_csv(filename, data):
  with open(filename, 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    for row in data:
      writer.writerow([row])

def load_csv(filename):
  data = []

  with open(filename, 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      v = [float(x) for x in row]
      data.append(v)

  return data

if __name__ == "__main__":
  # Preprocess data

  data = load_csv('data/training_set.csv')
  o = load_csv('data/training_labels.csv')
  write_libsvm(data, o, 'libsvm/tmp.libsvm')

  y, x = svm_read_problem("libsvm/tmp.libsvm")

  start_time = time.time()

  m = svm_train(y, x, "-c 4 -t 0 -n 0.2")

  train_time = time.time() - start_time

  data = load_csv('data/test_set.csv')
  o = [[0] for i in range(len(data))]
  # data = load_csv('data/validation_set.csv')
  # o = load_csv('data/validation_labels.csv')
  write_libsvm(data, o, 'libsvm/tmp.libsvm')

  y, x = svm_read_problem("libsvm/tmp.libsvm")

  start_time = time.time()

  p_label, p_acc, p_val = svm_predict(y, x, m)

  predict_time = time.time() - start_time

  print('Train: {}, Predict: {}'.format(train_time, predict_time)) 
  
  write_csv('data/predicted_other.csv', [int(x) for x in p_label])