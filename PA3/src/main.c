#include <stdlib.h>
#include "mat.h"
#include "ann.h"


#define FEATURES_N      354

#define DATA_SET_N      (1680 + 1800)
#define TRAINING_SET_N  1800
#define TEST_SET_N      701

#define ANN_HL_NODES    20 
#define ANN_MU          0.5

/**
 * @brief Reads a CSV file and converts it to a matrix structure mat_t
 * @details Reads a CSV file and converts it to a matrix structure mat_t
 *          The returned structure must be freed
 * 
 * @param filename  Input file name
 * @param rows      Number of lines in the file
 * @param cols      Number of values per line
 * @return mat_t*
 */
mat_t* mat_from_csvfile(const char* filename, int rows, int cols) {
  mat_t* m = mat_create(rows, cols);
  FILE* fp = fopen(filename, "r");
  char line[65535];

  int row = 0;
  while (row < rows && fgets(line, 65535, fp)) {
    int col = 0;
    char* v = strtok(line, ",");
    while (v != NULL) {
      MAT_SET(m, row, col, strtod(v, NULL));

      v = strtok(NULL, ",");
      col += 1;
    }
    row += 1;
  }

  fclose(fp);

  return m;
}

/**
 * @brief Encodes a matrix structure mat_t into a string and save to file
 * @details Encodes a matrix structure mat_t into a string and save to file
 * 
 * @param m         Input matrix
 * @param filename  Output file name
 */
void mat_to_csvfile(mat_t* m, const char* filename) {
  FILE* fp = fopen(filename, "w");
  int i, j;
  for (i = 0; i < m->rows; ++i) {
    for (j = 0; j < m->cols; ++j) {
      if (j > 0)
        fprintf(fp, ",");
      fprintf(fp, "%.0f", MAT_GET(m, i, j)); // note cast to int
    }
    fprintf(fp, "\r\n");
  }

  fclose(fp);
}

/**
 * @brief Trains an ANN with input training and validation data
 * @details Trains an ANN with input training and validation data
 * 
 * @param data_filename     File name of training data
 * @param label_filename    File name of training labels
 * @param datapoints        Number of lines in the training data
 * @param vdata_filename    File name of validation data
 * @param vlabel_filename   File name of validation label
 * @param vdatapoints       Number of lines in the validation data
 * @param features          Number of features
 * @param outputs           Number of output neurons
 * @return ann*
 */
ann* train(
    const char* data_filename, const char* label_filename, int datapoints, 
    const char* vdata_filename, const char* vlabel_filename, int vdatapoints,
    int features, int outputs) {
  ann* a = ann_create(features, outputs);

  ann_add_layer(a, ANN_HL_NODES);
  ann_add_layer(a, outputs);

  // Load training data
  mat_t* data = mat_from_csvfile(data_filename, datapoints, features);
  mat_t* labels = mat_from_csvfile(label_filename, datapoints, 1);

  // Load validation data
  mat_t* vdata = mat_from_csvfile(vdata_filename, vdatapoints, features);
  mat_t* vlabels = mat_from_csvfile(vlabel_filename, vdatapoints, 1);

  int i, j, maxiter = 1000;
  mat_t* epoch_j_cor = mat_create(maxiter, 2);
  for (i = 0; i < maxiter; ++i) {
    mat_t* sh = mat_create(1, datapoints);
    sh = mat_index_shuffle(sh);

    // Training step
    for (j = 0; j < datapoints; ++j) {
      int index = sh->data[j];

      mat_t* data_sub = mat_create_submat(data, index, 0, 1, features);

      mat_t* label_sub = mat_create(1, outputs);
      label_sub = mat_fill(label_sub, 0);
      MAT_SET(label_sub, 0, (int)MAT_GET(labels, index, 0) - 1, 1);

      mat_t* e = ann_train(a, data_sub, label_sub, ANN_MU);

      mat_free(&e);
      mat_free(&label_sub);
      mat_free(&data_sub);
    }

    mat_free(&sh);

    // Validation step
    double J = 0.;
    int correct = 0;
    for (j = 0; j < vdatapoints; ++j) {
      int index = j;

      mat_t* data_sub = mat_create_submat(vdata, index, 0, 1, features);
      mat_t* label_sub = mat_create(1, outputs);
      label_sub = mat_fill(label_sub, 0);
      MAT_SET(label_sub, 0, (int)MAT_GET(vlabels, index, 0) - 1, 1);

      mat_t* o = ann_feed(a, data_sub);

      int k, y = 0;
      for (k = 1; k < outputs; ++k) {
        if (MAT_GET(o, 0, k) > MAT_GET(o, 0, y)) {
          y = k;
        }
      }
      if (MAT_GET(label_sub, 0, y) == 1)
        correct += 1;

      mat_t* e = mat_sub(o, label_sub);
      e = mat_multiply(e, e);
      e = mat_multiply_scalar(e, 0.5f);

      J += mat_v_sum(e);

      mat_free(&e);
      mat_free(&label_sub);
      mat_free(&data_sub);
    }

    MAT_SET(epoch_j_cor, i, 0, i);
    MAT_SET(epoch_j_cor, i, 1, J);
    printf("Epoch: %d\tJ: %f\t Correct Guess: %d\n", i, J, correct);
    if (J < 0.01)
      break;
  }

  mat_to_csvfile(epoch_j_cor, "data/epoch_j_cor.csv");

  mat_free(&epoch_j_cor);
  mat_free(&vlabels);
  mat_free(&vdata);
  mat_free(&labels);
  mat_free(&data);

  return a;
}

/**
 * @brief Output predictions in a file using a trained ANN
 * @details Output predictions in a file using a trained ANN
 * 
 * @param a               Trained ANN
 * @param input_filename  File name of input data
 * @param datapoints      Number of lines in the input data
 * @param features        Number of features
 * @param outputs         Number of output neurons in ANN
 * @param output_filename File name of output
 */
void predict(ann* a,
    const char* input_filename, int datapoints, int features, int outputs,
    const char* output_filename) {
  mat_t* data = mat_from_csvfile(input_filename, datapoints, features);

  mat_t* predictions = mat_create(datapoints, 1);

  int i, j;
  for (i = 0; i < datapoints; ++i) {
    mat_t* data_sub = mat_create_submat(data, i, 0, 1, features);

    mat_t* o = ann_feed(a, data_sub);
    int y = 0;
    for (j = 1; j < outputs; ++j) {
      if (MAT_GET(o, 0, j) > MAT_GET(o, 0, y)) {
        y = j;
      }
    }

    MAT_SET(predictions, i, 0, y + 1);

    mat_free(&data_sub);
    mat_free(&o);
  }

  mat_to_csvfile(predictions, output_filename);

  mat_free(&predictions);
  mat_free(&data);
}

int main(int argc, char** argv) {
  srand(time(NULL));

  ann* a = train(
    "data/training_set.csv", "data/training_labels.csv", TRAINING_SET_N, 
    "data/validation_set.csv", "data/validation_labels.csv", DATA_SET_N - TRAINING_SET_N, 
    FEATURES_N, 8);

  predict(a, 
    "data/test_set.csv", TEST_SET_N, 
    FEATURES_N, 8, 
    "data/predicted_ann.csv");

  ann_free(&a);
}