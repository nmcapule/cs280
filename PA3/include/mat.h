// Author: Nathaniel Capule (nmcapule@gmail.com)
#ifndef __NATS_MAT_H__
#define __NATS_MAT_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define UTIL_FRAND() ((double)rand()/(double)(RAND_MAX))

#define MAT_DATA_SZ(m) (m->rows * m->cols)
#define MAT_RC_TO_I(m, row, col) ((row) * m->cols + (col))
#define MAT_I_TO_R(m, i) ((i) / m->cols)
#define MAT_I_TO_C(m, i) ((i) % m->cols)
#define MAT_I_TO_RC(m, i, p_row, p_col) \
  *p_row = MAT_I_TO_R(m, i); \
  *p_col = MAT_I_TO_C(m, i);
#define MAT_SET(m, r, c, d) m->data[MAT_RC_TO_I(m, r, c)] = d;
#define MAT_GET(m, r, c) m->data[MAT_RC_TO_I(m, r, c)]

typedef struct _mat {
  uint32_t rows, cols;
  double* data;
} mat_t;

void mat_print(mat_t* in) {
  int i, sz = MAT_DATA_SZ(in);
  printf("* ");
  for (i = 0; i < sz; ++i) {
    printf("%2.5f ", in->data[i]);
    if ((i + 1) % in->cols == 0)
      printf("\n");
    if ((i + 1) < sz)
      printf("  ");
  }
}

mat_t* mat_create(uint32_t rows, uint32_t cols) {
  mat_t* m = (mat_t*)malloc(sizeof(mat_t));

  m->rows = rows;
  m->cols = cols;
  m->data = (double*)malloc(sizeof(double) * MAT_DATA_SZ(m));

  return m;
}

mat_t* mat_create_submat(mat_t* in, uint32_t row, uint32_t col, uint32_t h, uint32_t w) {
  mat_t* m = mat_create(h, w);

  int i, j;
  for (i = 0; i < h; ++i) {
    for (j = 0; j < w; ++j) {
      MAT_SET(m, i, j, MAT_GET(in, i + row, j + col));
    }
  }

  return m;
}

mat_t* mat_create_identity(unsigned w) {
  mat_t* m = mat_create(w, w);

  uint32_t i, r, c, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    MAT_I_TO_RC(m, i, &r, &c);

    m->data[i] = (r == c)? 1: 0;
  }

  return m;
}

mat_t* mat_index_shuffle(mat_t* in) {
  mat_t* m = in;
  int i, max = MAT_DATA_SZ(m);

  for (i = 0; i < max; ++i) {
    m->data[i] = i;
  }

  for (i = 0; i < max; ++i) {
    int si = rand() % max;

    double t = m->data[i];
    m->data[i] = m->data[si];
    m->data[si] = t;
  }

  return m;
}

mat_t* mat_from_array(double* v, uint32_t len) {
  mat_t* m = mat_create(1, len);

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] = v[i];
  }

  return m;
}

mat_t* mat_clone(mat_t* in) {
  mat_t* m = mat_create(in->rows, in->cols);

  uint32_t i, sz = MAT_DATA_SZ(in);
  for (i = 0; i < sz; ++i) {
    m->data[i] = in->data[i];
  }

  return m;
}

mat_t* mat_clone_to(mat_t* dest, mat_t* in) {
  mat_t* m = dest;

  uint32_t i, sz = MAT_DATA_SZ(in);
  for (i = 0; i < sz; ++i) {
    m->data[i] = in->data[i];
  }

  return m;
}

void mat_free(mat_t** in) {
  free((*in)->data);
  free((*in));

  *in = NULL;
}

mat_t* mat_randomize(mat_t* in) {
  mat_t* m = in;

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] = UTIL_FRAND();
  }

  return m;
}

mat_t* mat_fill(mat_t* in, double x) {
  mat_t* m = in;

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] = x;
  }

  return m;
}

mat_t* mat_transpose(mat_t* in) {
  mat_t* m = mat_create(in->cols, in->rows);
  
  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    uint32_t sr, sc;
    MAT_I_TO_RC(in, i, &sr, &sc);

    MAT_SET(m, sc, sr, in->data[i]);
  }

  mat_free(&in);

  return m;
}

mat_t* mat_inverse(mat_t* in) {
  mat_t* m = in;

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] = 1 / m->data[i];
  }

  return m;
}

mat_t* mat_cross(mat_t* in, mat_t* other) {
  mat_t* m = in;

  // TODO

  return m;
}

mat_t* mat_dot(mat_t* in, mat_t* other) {
  mat_t* m = mat_create(in->rows, other->cols);

  if (other->rows != in->cols) {
    printf("Warning! Incompatible size!\n");
  }

  uint32_t i, r, c;
  for (r = 0; r < in->rows; ++r) {
    for (c = 0; c < other->cols; ++c) {
      double x = 0;
      for (i = 0; i < in->cols; ++i) {
        x += MAT_GET(in, r, i) * MAT_GET(other, i, c);
      }
      MAT_SET(m, r, c, x);
    }
  }

  mat_free(&in);

  return m;
}

mat_t* mat_multiply(mat_t* in, mat_t* other) {
  mat_t* m = in;

  if (other->rows != in->rows || other->cols != in->cols) {
    printf("Warning! Incompatible size!\n");
  }

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] *= other->data[i];
  }

  return m;
}

mat_t* mat_add(mat_t* in, mat_t* other) {
  mat_t* m = in;

  if (other->rows != in->rows || other->cols != in->cols) {
    printf("Warning! Incompatible size!\n");
  }

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] += other->data[i];
  }

  return m;
}

mat_t* mat_sub(mat_t* in, mat_t* other) {
  mat_t* m = in;

  if (other->rows != in->rows || other->cols != in->cols) {
    printf("Warning! Incompatible size!\n");
  }

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] -= other->data[i];
  }

  return m;
}

mat_t* mat_multiply_scalar(mat_t* in, double x) {
  mat_t* m = in;

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] *= x;
  }

  return m;
}

mat_t* mat_add_scalar(mat_t* in, double x) {
  mat_t* m = in;

  uint32_t i, sz = MAT_DATA_SZ(m);
  for (i = 0; i < sz; ++i) {
    m->data[i] += x;
  }

  return m;
}

double mat_v_sum(mat_t* in) {
  mat_t* m = in;

  int i, max = MAT_DATA_SZ(m);
  double sum = 0.;
  for (i = 0; i < max; ++i) {
    sum += m->data[i];
  }

  return sum;
}

mat_t* mat_v_extend(mat_t* in, mat_t* v) {
  mat_t* m = mat_create(1, in->cols + v->cols);

  int i, max = MAT_DATA_SZ(m);
  for (i = 0; i < in->cols; ++i) {
    m->data[i] = in->data[i];
  }
  for (i = 0; i < v->cols; ++i) {
    m->data[i + in->cols] = v->data[i];
  }

  return m;
}

mat_t* mat_v_extend_arr(mat_t* in, double* v, int len) {
  mat_t* t = mat_from_array(v, len);

  mat_t* m = mat_v_extend(in, t);

  mat_free(&t);

  return m;
}

mat_t* mat_v_extend_f(mat_t* in, double x) {
  mat_t* t = mat_create(1, 1);
  t->data[0] = x;

  mat_t* m = mat_v_extend(in, t);

  mat_free(&t);

  return m;
}

#endif