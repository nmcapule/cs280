// Author: Nathaniel Capule (nmcapule@gmail.com)
#ifndef __NATS_ANN_H__
#define __NATS_ANN_H__

#include <math.h>
#include "mat.h"
#include "math_ext.h"

typedef struct _ann_layer {
  mat_t *data;

  mat_t* input_cache;
  mat_t* output_cache;

  mat_t* delta_cache;

  int neurons;
  int inputs;

  double (*activation)(double);
} ann_layer;

typedef struct _ann_layer_lln {
  ann_layer* layer;
  struct _ann_layer_lln* next;
  struct _ann_layer_lln* prev;
} ann_layer_lln;

typedef struct _ann {
  ann_layer_lln* layer_list;

  int inputs;
  int outputs;
} ann;

ann_layer* ann_layer_create(int inputs, int neurons) {
  ann_layer* l = (ann_layer*)malloc(sizeof(ann_layer));

  inputs = inputs + 1; // for bias

  l->data = mat_create(inputs, neurons);
  l->data = mat_randomize(l->data);
  l->inputs = inputs;
  l->neurons = neurons;

  l->output_cache = mat_create(1, neurons);
  l->input_cache = mat_create(1, inputs);

  l->delta_cache = mat_create(inputs, neurons);

  l->activation = &math_ext_sigmoid;

  return l;
}

mat_t* ann_layer_feed(ann_layer* in, mat_t* v) {
  v = mat_v_extend_f(v, .5f); // for bias

  // copy to input_cache
  int i, max = MAT_DATA_SZ(v);
  for (i = 0; i < max; ++i)
    MAT_SET(in->input_cache, 0, i, v->data[i]);

  mat_t* m = mat_dot(v, in->data);

  max = MAT_DATA_SZ(m);
  for (i = 0; i < max; ++i) {
    m->data[i] = (*in->activation)(m->data[i]);

    // copy to output_cache
    MAT_SET(in->output_cache, 0, i, m->data[i]);
  }

  return m;
}

double ann_layer_calc_e(ann_layer* in, mat_t* t) {
  mat_t* tmp = mat_clone(t);
  tmp = mat_multiply_scalar(tmp, -1);
  tmp = mat_add(in->data, tmp);
  tmp = mat_multiply(tmp, tmp);

  double sum = 0;
  int i, max = MAT_DATA_SZ(tmp);
  for (i = 0; i < max; ++i) {
    sum += tmp->data[i];
  }

  mat_free(&tmp);

  return 0.5f * sum;
}

void ann_layer_free(ann_layer** in) {
  mat_free(&((*in)->data));
  mat_free(&((*in)->output_cache));
  mat_free(&((*in)->input_cache));
  free(*in);

  *in = NULL;
}

ann_layer_lln* ann_layer_lln_create(ann_layer *in) {
  ann_layer_lln* lln = (ann_layer_lln*)malloc(sizeof(ann_layer_lln));
  lln->layer = in;
  lln->next = NULL;
  lln->prev = NULL;

  return lln;
}

void ann_layer_lln_free(ann_layer_lln** in) {
  if (*in == NULL)
    return;
  ann_layer_lln_free(&((*in)->next));
  ann_layer_free(&((*in)->layer));

  *in = NULL;
}

ann* ann_create(int inputs, int outputs) {
  ann* a = (ann*)malloc(sizeof(ann));

  a->inputs = inputs;
  a->outputs = outputs;
  a->layer_list = NULL;

  return a;
}

ann_layer* ann_add_layer(ann* in, int neurons) {
  ann_layer_lln* ln = in->layer_list;
  while (ln != NULL && ln->next != NULL)
    ln = ln->next;

  int inputs = ln? ln->layer->neurons: in->inputs;

  ann_layer* l = ann_layer_create(inputs, neurons);
  ann_layer_lln* nln = ann_layer_lln_create(l);

  if (ln == NULL)
    in->layer_list = nln;
  else {
    ln->next = nln;
    nln->prev = ln;
  }

  return l;
}

mat_t* ann_feed(ann* in, mat_t* v) {
  ann_layer_lln* ln = in->layer_list;

  if (ln == NULL)
    return v;

  mat_t* m = mat_clone(v);
  while (ln != NULL) {
    m = ann_layer_feed(ln->layer, m);
    ln = ln->next;
  }

  return m;
}

mat_t* ann_layer_error_signal(mat_t* o, mat_t* y) {
  int i, max = MAT_DATA_SZ(o);
  for (i = 0; i < max; ++i) {
    o->data[i] = o->data[i] * (1 - o->data[i]) * (y->data[i] - o->data[i]);
  }

  return o;
}

mat_t* ann_layer_error_signal_h(mat_t* o, mat_t* nl_e, mat_t* nl_w) {
  // mat_t* t = mat_multiply(mat_clone(nl_e), nl_w);
  // mat_t* f = mat_create(t->cols, 1);
  // f = mat_fill(f, 1.);

  mat_t* t_e = mat_transpose(mat_clone(nl_e));
  mat_t* t = mat_dot(mat_clone(nl_w), t_e);

  // printf("Multiplying e and w\n");
  // mat_print(nl_e);
  // mat_print(nl_w);
  // mat_print(t);

  int i, max = MAT_DATA_SZ(o);
  for (i = 0; i < max; ++i) {
    o->data[i] = o->data[i] * (1 - o->data[i]) * (t->data[i]);
  }

  mat_free(&t_e);
  mat_free(&t);

  return o;
}

/**
 * @brief Train an ANN from a single data point and label
 * @details Train an ANN from a single data point and label
 * 
 * @param in    ANN to train
 * @param v     Input data point (vector)
 * @param y     Expected output of v
 * @param rate  Learning rate of ANN
 * @return error estimate matrix (for each output neuron)
 */
mat_t* ann_train(ann* in, mat_t* v, mat_t* y, double rate) {
  double momentum = 0.1;

  mat_t* o = ann_feed(in, v);

  // Get last layer (Output layer)
  ann_layer_lln* ln = in->layer_list;
  while (ln != NULL && ln->next != NULL) {
    ln = ln->next;
  }

  // Calculate output layer E
  // O * (1 - O) * (y - O)
  mat_t* o_e = ann_layer_error_signal(mat_clone(o), y);

  // Calculate momentum
  mat_t* mmd = mat_clone(ln->layer->delta_cache);
  mmd = mat_multiply_scalar(mmd, momentum);

  // Adjust output layer weights
  // error_delta = mu * x . e
  mat_t* o_wd = mat_transpose(
    mat_multiply_scalar(
      mat_dot(
        mat_transpose(mat_clone(o_e)), ln->layer->input_cache),
      rate));

  mat_t* h_e = o_e;
  mat_t* h_wd = o_wd;

  // momentum * delta_cache
  mat_clone_to(ln->layer->delta_cache, h_wd);
  
  // w = w + error_delta + momentum_delta
  ln->layer->data = mat_add(ln->layer->data, mmd);
  ln->layer->data = mat_add(ln->layer->data, h_wd);

  mat_free(&mmd);

  // Calculate same for hidden layers
  while (ln != NULL && ln->prev != NULL) {
    ln = ln->prev;
    mat_t* p_e = h_e;

    h_e = ann_layer_error_signal_h(
      mat_clone(ln->layer->output_cache), 
      p_e,
      ln->next->layer->data);
    mat_free(&p_e);

    mat_t* mmd = mat_clone(ln->layer->delta_cache);
    mmd = mat_multiply_scalar(mmd, momentum);

    h_wd = mat_transpose(
      mat_multiply_scalar(
        mat_dot(
          mat_transpose(mat_clone(h_e)), ln->layer->input_cache),
        rate));

    // momentum * delta_cache
    mat_clone_to(ln->layer->delta_cache, h_wd);
    
    ln->layer->data = mat_add(ln->layer->data, mmd);
    ln->layer->data = mat_add(ln->layer->data, h_wd);
    // mat_print(ln->layer->data);s
    mat_free(&mmd);
  }

  mat_free(&h_e);
  mat_free(&h_wd);

  // int i, max = MAT_DATA_SZ(o);
  // for (i = 0; i < MAT_DATA_SZ(o); ++i) {
  //   o->data[i] = math_ext_round(o->data[i]);
  // }

  // we do not dealloc o, it is used on output
  mat_t* e = mat_sub(o, y);
  e = mat_multiply(e, e);
  e = mat_multiply_scalar(e, 0.5f);

  return e;
}

void ann_free(ann** in) {
  ann_layer_lln_free(&((*in)->layer_list));
  free(*in);

  *in = NULL;
}

#endif