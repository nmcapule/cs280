// Author: Nathaniel Capule (nmcapule@gmail.com)
#ifndef __NATS_UTIL_H__
#define __NATS_UTIL_H__

#include <math.h>

double math_ext_sigmoid(double x) {
  return 1. / (1 + exp(-x));
}

double math_ext_round(double x) {
  return (x >= 0.5)? 1: 0;
}

#endif